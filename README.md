# Jenkins | Share Library

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Context
Create a shared library to factorize the Slack notification step discussed in [lab7](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab7-test-fonctionnement).

## Requierements
1. Set up a CI/CD pipeline using Jenkins: [reference here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab5-deploiement).

2. Configure badges in the pipeline: [reference here](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab7-test-fonctionnement).


## Configuration of the shared library

### Creation of the .groovy function on GitHub
1. Create a new shared-library project.
> ![alt text](img/image.png)

2. Create a `vars` directory.
   shared-library > + New directory
> ![alt text](img/image-1.png)
   *Click on "Creating a new file"*

   Enter `vars/` to create a directory.
> ![alt text](img/image-3.png)
   *Creation of vars/*


3. Create a new file "SlackNotifier.groovy" and enter the following notification code: [Code here](https://github.com/CarlinFongang/shared-library.git)
````
#!/usr/bin/env groovy

def call(String buildResult) {
  if ( buildResult == "SUCCESS" ) {
    slackSend color: "good", message: "CONGRATULATION: Job ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was successful ! more info ${env.BUILD_URL}"
  }
  else if( buildResult == "FAILURE" ) { 
    slackSend color: "danger", message: "BAD NEWS:Job ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was failed ! more info ${env.BUILD_URL}"
  }
  else if( buildResult == "UNSTABLE" ) { 
    slackSend color: "warning", message: "BAD NEWS:Job ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was unstable ! more info ${env.BUILD_URL}"
  }
  else {
    slackSend color: "danger", message: "BAD NEWS:Job ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} its result was unclear ! more info ${env.BUILD_URL}"	
  }
}
````
>![alt text](img/image-4.png)
*Writing the Groovy file*

Once this configuration is done, commit the file.

>![alt text](img/image-5.png)

### Configuring Jenkins to use the shared library
Dashboard > Administer Jenkins > System > Global Pipeline Libraries > Add
>![alt text](img/image-6.png)

1. Configure the pipeline library
Enter the values `Name`: for the library name `Default version`: for the version
>![alt text](img/image-7.png)

2. Provide the source repository access for the .groovy file
>![alt text](img/image-8.png)
*Providing the "Shared library" repository*

### Modifying the Jenkinsfile to incorporate the .groovy file for the Shared Library

1. Importing the "Shared Library" into Jenkinsfile
```groovy
/* import shared library */
@Library('carlinfg-shared-library')_
```

>![alt text](img/image-10.png)
*Adding the import script at the beginning of the manifest*

2. Adding the "post" section for implementing Slack notifications
This script allows the use of the imported "Shared Library" to send Slack notifications. Add it at the end of the manifest after all stages are completed.
```groovy
  post {
    always {
      script {
        SlackNotifier currentBuild.result
      }
    }  
  }
```

>![alt text](img/image-11.png)
*Adding the post section*

### Result
1. Execution output of the pipeline
>![alt text](img/image-12.png)

2. Slack Notifications
>![alt text](img/image-13.png)
*Receiving Slack notifications*






